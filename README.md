# TaxMax

Taxmax creates simple heatmaps to show the taxonomic similarity between NCBI taxids from BLAST results.
It fetches the required lineages from NCBI and compares them. It uses a simple scoring system between 0 and 1 by comparing the lineages until they diverge. Equal taxids are scored as 1, totally different lineages are scored as 0.


## Get taxmax

 - `git clone https://gitlab.com/janpb/taxmax.git`

## Limitations
So far, taxmax has only one use. Further, It's rather convoluted to use. However, it's still under development.


## Inputs
Taxmax needs two lists of NCBI taxids.

Currently, taxmax requires three inputs:

    - alias list: This is a tab delimited list with two columns:

         alias\tNCBI_sequence_accession

    -hittaxids: lsit of taxids to comapare

    - the third argument is read from STDIN and is a lsit with two columns:
        alias blast_result_taxid

## Usage

- `src/taxmax.py -h`
- `src/taxmax.py -s example/alias_file.example -t example/hittaxis.example < example/blast_results.example`