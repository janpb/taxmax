#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  -------------------------------------------------------------------------------
#  \file sql2heatmap.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.0.0
#  \description
#  -------------------------------------------------------------------------------


import io
import os
import sys
import numpy
import argparse
import matplotlib.pyplot

sys.path.insert(1, os.path.join(sys.path[0], '../include/ncbi/src/'))
import edirect.callimachus.ncbi_callimachus
import edirect.edbase.edanalyzer

sys.path.insert(1, os.path.join(sys.path[0], '../include/taxonomy/src/'))
import taxonomist

class TaxonomyAnalyzer(edirect.edbase.edanalyzer.EdAnalyzer):

  def __init__(self):
    super().__init__()
    self.taxonomist = taxonomist.NcbiTaxonomist()
    self.queries = {}

  def analyze_result(self, reponse, request):
    self.taxonomist.parse(reponse)

  def parse_hittaxids(self, hittaxids):
    fh = open(hittaxids, 'r')
    for i in fh:
      self.queries[i.rstrip()] = 0
    fh.close()

  def fetch_lineages(self, taxids):
    nc = edirect.callimachus.ncbi_callimachus.NcbiCallimachus(email='jan')
    qry = nc.new_query()
    qid = qry.add_query(qry.new_fetch(analyzer=self,
                                      options={'db' : 'taxonomy',
                                               'retmode' : 'xml',
                                               'id' : taxids}))
    nc.run_query(qry)

class SequenceAnalyzer(edirect.edbase.edanalyzer.EdAnalyzer):

  class Sequence:

    def __init__(self, accession, alias):
      self.accession = accession
      self.alias = alias
      self.taxid = None
      self.lineage = {}

    def add_lineage(self, lineage):
      for i in lineage:
        self.lineage[i.rank] = i

  def __init__(self):
    super().__init__()
    self.aliasmap = {}
    self.sequencemap = {}
    self.taxonomist = taxonomist.NcbiTaxonomist()

  def parse_seqlist(self, seqlist):
    fh = open(seqlist, 'r')
    for i in fh:
      alias, accession = i.rstrip().split('\t')
      if alias not in self.aliasmap:
        self.aliasmap[alias] = self.Sequence(accession, alias)
      if accession not in self.sequencemap:
        self.sequencemap[accession] = []
      self.sequencemap[accession].append(alias)
    fh.close()

  def fetch_sequence_info(self):
    nc = edirect.callimachus.ncbi_callimachus.NcbiCallimachus(email='jan')
    qry = nc.new_query()
    qid = qry.add_query(qry.new_fetch(analyzer=self,
                                      options={'db' : 'nuccore',
                                               'retmode' : 'json',
                                               'rettype' : 'docsum',
                                               'id' : [self.aliasmap[x].accession for x in self.aliasmap]}))
    nc.run_query(qry)

  def analyze_result(self, response, request):
    for i in response['result']['uids']:
      if response['result'][i]['caption'] in self.sequencemap:
        for j in self.sequencemap[response['result'][i]['caption']]:
          self.aliasmap[j].taxid = response['result'][i]['taxid']

  def assign_lineages(self):
    for i in self.aliasmap:
      clade = self.taxonomist.get_clade_from_taxid(self.aliasmap[i].taxid)
      lin = self.taxonomist.assemble_lineage_from_taxid(self.aliasmap[i].taxid)
      self.aliasmap[i].add_lineage(self.taxonomist.get_normalized_clade_lineage(clade, lin))

class TaxaMatrixCreator:

  class HitTaxa:
    def __init__(self, taxid):
      self.taxid = int(taxid)
      self.lineage = {}

    def add_lineage(self, lineage):
      for i in lineage:
        self.lineage[i.rank] = i

  def __init__(self):
    self.hitmap = {}
    self.aliasmap = {}
    self.taxonomist = taxonomist.NcbiTaxonomist()

  def load_sequences(self, seq_alias_file):
    s = SequenceAnalyzer()
    s.parse_seqlist(seq_alias_file)
    s.fetch_sequence_info()
    return s

  def load_hittaxids(self, hittaxids):
    fh = open(hittaxids, 'r')
    for i in fh:
      if i.rstrip() not in self.hitmap:
        self.hitmap[i.rstrip()] = self.HitTaxa(i.rstrip())
    fh.close()

  def fetch_lineages(self, sequences):
    t = TaxonomyAnalyzer()
    queries = {}
    queries.update({sequences.aliasmap[x].taxid:0 for x in sequences.aliasmap})
    queries.update({x:0 for x in self.hitmap})
    t.fetch_lineages([x for x in queries])

  def assign_lineages(self, sequences):
    sequences.assign_lineages()
    for i in self.hitmap:
      clade = self.taxonomist.get_clade_from_taxid(i)
      lin = self.taxonomist.assemble_lineage_from_taxid(i)
      self.hitmap[i].add_lineage(self.taxonomist.get_normalized_clade_lineage(clade, lin))

  def get_taxsimilarity(self, lineageA, lineageB):
    score = 0
    score_step = 1 / len(lineageA)
    for i in lineageA:
      if lineageA[i].rank in lineageB:
        if lineageA[i].taxid != lineageB[i].taxid:
          return score
        score += score_step
      else:
        return score
    return score

  def color_lables(self, rank='group'):
    colormap = {}
    labelmap = {}
    for i in self.hitmap:
      labelmap[self.hitmap[i].taxid] = [None, None]
      labelmap[self.hitmap[i].taxid][0] = self.hitmap[i].lineage.get('superkingdom', None)
      labelmap[self.hitmap[i].taxid][1] = self.hitmap[i].lineage.get(rank, None)
    #print(labelmap)

  def make_matrix(self, sequences):
    taxsimil = {}
    cols = {}
    rows = {}
    rowlabels = []
    collabels = []

    for i in sys.stdin:
      alias, hittaxid = i.rstrip().split('\t')
      if hittaxid not in cols:
        cols[hittaxid] = 0
        collabels.append(hittaxid)
      if alias not in rows:
        rows[alias] = 0
        rowlabels.append(alias)
      if sequences.aliasmap[alias].taxid == self.hitmap[hittaxid].taxid:
        taxsimil[hittaxid] = {alias : 1}
      else:
        taxsimil[hittaxid] = {alias : self.get_taxsimilarity(sequences.aliasmap[alias].lineage, self.hitmap[hittaxid].lineage)}

    hmap = []
    for i in rowlabels:
      hmap.append([])
      for j in collabels:
        if i in taxsimil[j]:
          hmap[-1].append(taxsimil[j][i])
        else:
          hmap[-1].append(0)
    self.color_lables()
    fig, ax = matplotlib.pyplot.subplots()
    im = ax.imshow(hmap, cmap=matplotlib.pyplot.cm.get_cmap('Blues'))
    ax.set_xticks(numpy.arange(len(collabels)))
    ax.set_xticklabels(collabels)
    ax.set_yticks(numpy.arange(len(rowlabels)))
    ax.set_yticklabels(rowlabels)
    ax.tick_params(top=True, bottom=False, labeltop=True, labelbottom=False)
    matplotlib.pyplot.setp(ax.get_xticklabels(), rotation=90, ha="center")
    for edge, spine in ax.spines.items():
      spine.set_visible(False)
    ax.set_xticks(numpy.arange(len(collabels))-0.5, minor=True)
    ax.set_yticks(numpy.arange(len(rowlabels))-0.5, minor=True)
    ax.grid(which="major", color="gray", linestyle=':', linewidth=0.5)
    ax.grid(which="minor", color="white", linestyle='-', linewidth=2)
    ax.tick_params(which="minor", bottom=False, left=False)
    matplotlib.pyplot.savefig("taxsimil.pdf", bbox_inches="tight")
    cbar = ax.figure.colorbar(im, ax=ax)
    cbar.ax.set_ylabel('Lineage similarity', rotation=-90, va="bottom")
    matplotlib.pyplot.savefig("taxsimil.pdf", bbox_inches="tight")


def main():
  ap = argparse.ArgumentParser(description='Taxmax')
  ap.add_argument('-s','--sequences', type=str, help='Alias-Sequence file, tab-delimited')
  ap.add_argument('-t','--hittaxids', type=str, help='Taxids for blast hits')
  args = ap.parse_args()

  tm = TaxaMatrixCreator()
  sequences = tm.load_sequences(args.sequences)
  tm.load_hittaxids(args.hittaxids)
  tm.fetch_lineages(sequences)
  tm.assign_lineages(sequences)
  tm.make_matrix(sequences)
  return 0

if __name__ == '__main__':
  main()
