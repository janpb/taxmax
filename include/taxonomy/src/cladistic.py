#  -----------------------------------------------------------------------------
#  File: cladistic.py
#  Author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  Copyright: 2018 The University of Sydney
#  Version: 0
#  Description:
#  -----------------------------------------------------------------------------
from clade import basic_clade
from clade import virus_clade

class Cladistic:

  eukaryota_ranks = ['superkingdom', 'kingdom', 'phylum', 'subphylum', 'class',
                     'superorder', 'order', 'suborder', 'infraorder',
                     'parvorder', 'superfamily', 'family', 'subfamily', 'genus',
                     'subgenus', 'species', 'subspecies']

  bacteria_ranks = ['superkingdom', 'phylum', 'class', 'order', 'family',
                    'genus', 'species']

  viroid_ranks = ['superkingdom', 'family', 'genus', 'species']

  virus_ranks = ['superkingdom', 'group', 'order', 'family', 'subfamily',
                 'genus', 'species']
  unknown_ranks = ['superkingdom', 'subspecies', 'species']
  rankmap = {'bacteria' : bacteria_ranks, 'archaea' : bacteria_ranks,
             'eukaryota' : eukaryota_ranks, 'viroids' : viroid_ranks,
             'unknown' : unknown_ranks, 'viruses': virus_ranks}

  def __init__(self):
    self.clades = {}

  def characterize(self, taxa):
    superkingdom = self.identify_clade(taxa)
    if not self.cladeExists(superkingdom):
      if superkingdom == 'viruses':
        self.clades[superkingdom] = virus_clade.VirusClade(Cladistic.virus_ranks)
      elif superkingdom in Cladistic.rankmap:
        self.clades[superkingdom] = basic_clade.BasicClade(superkingdom,
                                                           Cladistic.rankmap[superkingdom])
      else:
        self.clades[superkingdom] = basic_clade.BasicClade('unknown',
                                                           Cladistic.rankmap['unknown'])
    self.clades[superkingdom].add_lineage(taxa)
    return self.clades[superkingdom]

  def cladeExists(self, cladename):
    return cladename in self.clades

  def get_clade(self, cladename):
    return self.clades[cladename]

  def identify_clade(self, taxa):
    for i in taxa:
      if i.rank.lower() == 'superkingdom':
        return i.name.lower()
    return 'unknown'

  def get_clades(self):
    return  self.clades
