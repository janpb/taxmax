#-------------------------------------------------------------------------------
#  \file virus_clade.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 1.0.2
#  \bug The Baltimore classification is hacky and could use some better logic
#  \description Virus clade  inherits from Clade.
#               The default hierarchy is adapted for viral taxa according to
#               ICTV [0]. NCBI does not distinguished between DNA reverse
#               transcribing viruses (Baltimore 6, Hepadnaviridae) and DNA
#               reverse transcribing viruses (Baltimore 7, Caulimoviridae).
#               This class uses the corresponding taxid to make this
#               distinction (taken from [1]). It adds the rank 'group', which is
#               not an official NCBI rank.
#               References:
#                 [0]: http://www.ictvonline.org/virusTaxInfo.asp
#                 [1]: http://viralzone.expasy.org/all_by_species/235.html
#-------------------------------------------------------------------------------
from . import basic_clade

class VirusClade(basic_clade.BasicClade):

  baltimore = {
                35237  : 1, # dsDNA viruses, no RNA stage
                29258  : 2, # ssDNA viruses
                35325  : 3, # dsRNA viruses
                35278  : 4, # ssRNA positive-strand viruses, no DNA stage
                35301  : 5, # ssRNA negative-strand viruses
                35268  : 6, # Retro-transcribing viruses
                10404  : 7, # DNA reverse transcribing viruses, Hepadnaviridae
                186534 : 7, # DNA reverse transcribing viruses, Caulimoviridae
              }

  def __init__(self, ranklist):
    super().__init__('viruses', ranklist)

  def add_lineage(self, taxa):
    for i in range(len(taxa)):
      if taxa[i].taxid in VirusClade.baltimore and ( taxa[i].rank != 'group' and taxa[i].rank != 'family'):
        taxa[i].rank = 'group'
        taxa[i].name += " ({})".format(VirusClade.baltimore[taxa[i].taxid])
    self.lineages.append(taxa)
