#  -----------------------------------------------------------------------------
#  File: clade.py
#  Author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  Copyright: 2018 The University of Sydney
#  Version: 1.0.1
#  Description: The base class for clades. Implements methods which can be
#               applied to all clades.
#  -----------------------------------------------------------------------------
import sys

sys.path.insert(1, '../')
import taxon.basic_taxon

class BasicClade:

  def __init__(self, name, ranklist):
    self.ranks = {ranklist[x] : x for x in range(len(ranklist))}
    self.name = name
    self.lineages = []

  def add_lineage(self, taxa):
    self.lineages.append(taxa)

  def get_last_lineage(self):
    return self.lineages[-1]

  def isNormalizedRank(self, taxon):
    return taxon.rank in self.ranks

  def get_normalized_lineage(self, taxa):
    norm = [taxon.basic_taxon.BasicNcbiTaxon() for x in range(len(self.ranks))]
    #seen = {**self.ranks} # PEP 448, >= Python 3.5
    seen = dict(self.ranks) # < Python 3.5
    for i in taxa:
      if self.isNormalizedRank(i):
        norm[self.ranks[i.rank]] = i
        seen.pop(i.rank)
    for i in seen:
      norm[seen[i]].rank = i
      norm[seen[i]].name = 'NA'
    return norm
