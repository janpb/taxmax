#-------------------------------------------------------------------------------
#  \file: taxonomy_db.py
#  \author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright  GNU Lesser General Public License
#  \version: 1.0.1
#  \description SQLite handling for taxonomic data
#-------------------------------------------------------------------------------
import sys
import sqlite3

## TaxonomyDb
# @ingroup taxonomist
# TaxonomyDb handles SQLite3 taxonomy databases and consists of one or several
# TaxonomyTable instances.
class TaxonomyDb:

  #rankmap = {'group' : 'grp', 'order' : 'ordr'}
  # taxonomy tables registry. Static since taxonomies can be shared since they
  # have to be unique.
  tables = {}

  ## Constructor
  # Initialize a sqlite3 database in memory
  def __init__(self):
    self.name = ':memory:'
    self.conn = sqlite3.connect(self.name)

  ## Getter for database tables
  # @param table_name, str, name of table instance to get
  # @return table, table instance, None if doesn't exist
  def get_table(self, table_name):
    if self.tableExists(table_name):
      return self.tables[table_name]
    return None

  ## Check if table exists
  # @param table_name, str, name of table
  # @return boolean, True if exists, else False
  def tableExists(self, table_name):
    return table_name in TaxonomyDb.tables

  ## Helper function to register tables
  # @param table, table instance
  def register_table(self, table):
    self.tables[table.name] = table

  ## Helper function to execute sqlite3 statements
  # @param smtm, str, sqlite3 statement
  # @return execution instance
  def execute_stmt(self, stmt, values=None):
    c = self.conn.cursor()
    if values == None:
      return c.execute(stmt)
    return c.execute(stmt, values)

  ## Sqlite3 dumper function. Dump database to file or stdout. If no filename is
  # given, use stdout
  # @param sink, str, filename
  def dump(self, sink=None):
    if sink == None:
      for i in self.conn.iterdump():
        print(i, file=sys.stdout)
    else:
      fh = open(sink, 'w')
      for i in self.conn.iterdump():
        fh.write(i+'\n')
      fh.close()
