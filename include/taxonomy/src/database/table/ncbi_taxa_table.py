#  -----------------------------------------------------------------------------
#  File: ncbi_taxa_table.py
#  Author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  Copyright: 2018 The University of Sydney
#  Version: 0
#  Description: Table class handling NCBI taxa
#  -----------------------------------------------------------------------------

from . import taxonomy_table

class NcbiTaxaTable(taxonomy_table.TaxonomyTable):

  def __init__(self, database):
    super().__init__('ncbitaxa', database)

  def create(self):
    c = self.database.conn.cursor()
    stmt = """CREATE TABLE ncbitaxa
              (
                id           INT PRIMARY KEY,
                taxid        INT,
                rank         TEXT,
                name         TEXT,
                parent_taxid INT NULL,
                synonym      TEXT NULL,
                acronym      TEXT NULL,
                FOREIGN KEY (parent_taxid) REFERENCES ncbitaxa(taxid),
                UNIQUE(taxid)
              )"""
    c.execute(stmt)
    return self

  def insert_taxa(self, taxa):
    c = self.database.conn.cursor()
    stmt = """INSERT OR IGNORE INTO ncbitaxa (taxid, rank, name, parent_taxid, synonym, acronym)
                          VALUES (?,?,?,?)"""
    #print(stmt, (taxon.taxid, taxon.rank, taxon.name, taxon.parent_taxid))
    c.executemany(stmt, [(x.taxid, x.rank, x.name, x.parent_taxid, x.synonym, x.acronym) for x in taxa])
    self.commit()

  def get_lineage(self, taxid, normalized=False):
    stmt = """WITH RECURSIVE parent(taxid) AS
              (
                SELECT taxid                    \
                FROM ncbitaxa                   \
                WHERE taxid={}                  \
                UNION                           \
                SELECT n.parent_taxid           \
                FROM ncbitaxa n, parent         \
                WHERE n.taxid=parent.taxid
              )
              SELECT n.taxid,                   \
                     n.name,                    \
                     n.rank,                    \
                     n.parent_taxid             \
                     n.synonym                  \
                     n.acronym                  \
              FROM ncbitaxa n                   \
              JOIN parent p ON n.taxid=p.taxid  \
              """.format(taxid)
    if normalized:
      stmt += "WHERE n.rank != 'no rank'"
    c = self.database.conn.cursor()
    return c.execute(stmt)
