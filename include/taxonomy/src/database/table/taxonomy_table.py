#-------------------------------------------------------------------------------
#  \file taxonomy_table.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright  GNU Lesser General Public License
#  \version 1.0.1
#  \description Basic class for taxonomy tables
#-------------------------------------------------------------------------------
#import sqlite3

## TaxonomyTable
# @ingroup taxonomist
# TaxonomyTable is the main class to deal with NCBI taxonomy tables. Table
# instancs are registered by TaxonomyDb. It acts as very simple base class.

class TaxonomyTable:

  ## static rankmap to map taxonokmy names which clash with sqlite3 keywords
  rankmap = {'group' : 'grp', 'order' : 'ordr'}

  ## Constructor
  # @param name, str, clade name
  # @param database, reference, reference to corresponding sqlite3 instance
  def __init__(self, name=None, database=None):
    self.name = name
    self.database = database

  ## Creating table
  # A virtual function
  def create(self, stmt):
    raise NotImplementedError("Implement create() method")

  ## Helper function to commit changes
  def commit(self):
    self.database.conn.commit()

  ## Getter function to get static rankmap
  # @returns rankmap, dict
  def get_col_rank(self, rank):
    return self.__class__.rankmap.get(rank, rank)
