#-------------------------------------------------------------------------------
# \file taxonomy.py
# \author: Jan P Buchmann <lejosh@members.fsf.org>
# \date 2018
# \copyright  GNU Lesser General Public License
# \version 1.0.1
# \bug Handling the query taxid's is not yet fully established
# \brief NCBI Taxonomist parses and analyzes NCBI taxonomies.
# \details  The class parses the NCBI taxonomy XML files. Based on the NCBI
#           taxid it can assemble lineages and group several lineages into
#           clades.  Lineages consist of taxons. Each parsed taxon is stores in
#           a static database and  knows its parent and children. This allows
#           to assembles lineages on the fly.
#           The first encountered taxon in a NCBI taxonomy XML file is
#           considered a query and stored separately and can be used to handle
#           EDirect requests.
#
#           Lineages are a list of taxons linked by their parent. These lists
#           can be grouped into clades, e.g. eukaryote or virus. Clades are
#           specific classes and offer functions dealeing with lineages within
#           a clade.
#-------------------------------------------------------------------------------
import xml.etree.ElementTree as ET
import json

import taxon.basic_taxon
import database.taxonomy_db
import database.table.clade_table
import database.table.ncbi_taxa_table
import cladistic

## NcbiTaxonomist
# @ingroup taxonomist
# NcbiTaxonomist is the main class to deal with NCBI taxonomies. It stores
# all encountered taxons in a static dictionary. It is considered as a
# "singleton", i.e. only one Taxonomist should be instantiated for an analysis.
class NcbiTaxonomist:
  ## Taxa database
  # Static dictionary for all taxons obejcts
  taxa = {}
  aliases = {}

  ## Database handling
  # A static instance of database.taxonomy_db.TaxonomyDb()
  database = database.taxonomy_db.TaxonomyDb()

  ## Clade handling
  # A static instance of cladistic.Cladistic()
  cladistic = cladistic.Cladistic()

  ## Constructor
  def __init__(self):
    self.collect_aliases = False

  def update_phylogeny(self, taxon):
    if taxon.taxid not in NcbiTaxonomist.taxa:
      NcbiTaxonomist.taxa[taxon.taxid] = taxon
    if taxon.hasAlias():
      NcbiTaxonomist.aliases[taxon.get_alias()] = taxon.taxid
      NcbiTaxonomist.taxa[taxon.get_alias()] = taxon
    return NcbiTaxonomist.taxa[taxon.taxid]

  ## NCBI taxonomy XML parser
  # @param xml NCBI taxonomy XML, file-like object
  # @param t, Taxon object, Initialized taxon
  # @return dict, queries
  def parse(self, xml, t=taxon.basic_taxon.BasicNcbiTaxon()):
    qry = taxon.basic_taxon.BasicNcbiTaxon()
    isLineage = False
    parent = None
    queries = {}
    for event, elem in ET.iterparse(xml, events=["start", "end"]):
      if event == 'start' and elem.tag == 'LineageEx':
        isLineage = True
      if event == 'end' and elem.tag == 'LineageEx':
        isLineage = False
      if not isLineage:
        if elem.tag == 'Taxon' and event == 'start':
          parent = None
          qry = taxon.basic_taxon.BasicNcbiTaxon()
        elif elem.tag == 'Taxon' and event == 'end':
          self.update_phylogeny(qry)
          queries[qry.taxid] = 0
          qry.add_child(t)
        elif elem.tag == 'TaxaSet':
          continue
        else:
          self.parse_taxon(event, elem, qry)

      if isLineage:
        if elem.tag == 'Taxon' and event == 'start':
          t = taxon.basic_taxon.BasicNcbiTaxon()
        elif elem.tag == 'Taxon' and event == 'end':
          if parent != None:
            t.parent_taxid = parent.taxid
            parent.add_child(t)
          parent = self.update_phylogeny(t)
        else:
          self.parse_taxon(event, elem, t)
    return queries

  ## Method to parse a taxon node in NCBI Taxonomy XML entries
  # Storing required tags to reconstruct the lineage. The values are stored
  # directly in the correspondign taxon.
  # @param event, xml.etree.ElementTree event, 'start' or 'stop'
  # @param elem, xml.etree.ElementTree element, the XML tag
  # @param taxon, blib taxonomy taxon instance, either BasicTaxon or QueryTaxon
  def parse_taxon(self, event, elem, taxon):
    if event == 'start' and elem.tag == 'AkaTaxIds':
      self.collect_aliases = True
    if event == 'end':
      if elem.tag == 'TaxId' and not self.collect_aliases:
        taxon.taxid = int(elem.text)
      if elem.tag == 'TaxId' and self.collect_aliases:
        taxon.add_alias(elem.text)
      if elem.tag == 'ScientificName':
        taxon.name = elem.text
      if elem.tag == 'Synonym':
        taxon.synonym = elem.text
      if elem.tag == 'Acronym':
        taxon.acronym = elem.text
      if elem.tag == 'Rank':
        taxon.rank = elem.text
      if elem.tag == 'ParentTaxId':
        taxon.parent_taxid = int(elem.text)
      if elem.tag == 'Division':
        taxon.division = elem.text
      if elem.tag == 'GCName':
        taxon.genetic_code = elem.text
      if elem.tag == 'GCId':
        taxon.genetic_code = int(elem.text)
      if elem.tag == 'MGCName':
        taxon.mitochondrial_code = elem.text
      if elem.tag == 'MGCId':
        taxon.mitochondrial_code_id = int(elem.text)
      if elem.tag == 'CreateDate':
        taxon.create_date = elem.text
      if elem.tag == 'UpdateDate':
        taxon.update_date = elem.text
      if elem.tag == 'PubDate':
        taxon.pubdate = elem.text
      if elem.tag == 'PubDate':
        taxon.pubdate = elem.text
      if elem.tag == 'AkaTaxIds':
        self.collect_aliases = False

  ## Function to assemble a lineage from a taxid
  # @param taxid, NCBI_Taxid
  # @return taxa, a list of taxon instances for lineage
  def assemble_lineage_from_taxid(self, taxid):
    taxon = NcbiTaxonomist.taxa[int(taxid)]
    taxa = []
    while taxon.parent_taxid != None:
      taxa.insert(0, taxon)
      taxon = NcbiTaxonomist.taxa[taxon.parent_taxid]
    taxa.insert(0, taxon)
    return taxa

  ## Function to determine the clade for taxid
  # The lineage is assembled from the taxid and Cladistic determines the clade
  # of the lineage
  # @param taxid, int, NCBI Taxid
  # @return Clade instance or None
  def get_clade_from_taxid(self, taxid):
    taxa = self.assemble_lineage_from_taxid(int(taxid))
    return self.cladistic.characterize(taxa)

  ## Helper function to obtain a specific clade from Cladistic
  # @param clade, string, clade name
  # @return clade instance or None
  def get_clade(self, clade):
    if self.cladistic.cladeExists(clade):
      return self.cladistic.get_clade(clade)
    return None

  ## Helper function to obtain a specific clade from Cladistic
  # @param clade, string, clade name
  # @return clade instance or None
  def get_clades(self, clades=None):
    if clades == None:
      if len(self.cladistic.clades) > 0:
        return self.cladistic.get_clades()
    else:
      clades = {}
      for i in clades:
        if self.cladistic.cladeExists(i):
          clades[i] = self.cladistic.get_clade(i)
      return clades
    return None

  ## Resolve aliases for taxids
  # @param, taxid to resolve
  # @return taxid linking to input taxid or None
  def resolve_alias(self, src_taxid):
    if src_taxid in NcbiTaxonomist.aliases:
      return NcbiTaxonomist.taxa[NcbiTaxonomist.aliases[src_taxid]]
    return None

  ## Export a list of taxa , e.g. a lineage, in JSON
  # @param, list, taxa instances
  # @return Taxons in JSON format
  def export_json(self, taxa):
    return json.dumps([{'rank' : x.rank, 'taxid' : x.taxid, 'name' : x.name} for x in taxa])

  ## Dump a list of taxa names, e.g. a lineage, as a delimited line
  # @param taxa, list, list of taxa, e.g. lineage
  # @param delimiter, string/character, delimiter
  # @return string of taxa names separated by delimiter
  def dump(self, taxa, delimiter='\t'):
    return delimiter.join(str(i.name) for i in taxa)

  ## Store lineages into a SQLite3 database
  # @param clade_names,  list,  name of clades to store. If None, store all
  def store_clades(self, clade_names=None):
    clades = []
    if clade_names == None:
      clades = self.cladistic.get_clades()
    else:
      clades = [self.cladistic.get_clade(i) for i in clade_names]
    if len(clades) == 0:
      print("No clades found. Prepare clades, see get_clades_from_taxid()", file=sys.stderr)
      sys.exit()
    for i in clades:
      if not NcbiTaxonomist.database.tableExists(i.name):
        table = database.table.clade_table.CladeTable(i.name, NcbiTaxonomist.database)
        table.create(i)
      if  NcbiTaxonomist.database.get_table(i.name) != None:
        table = NcbiTaxonomist.database.get_table(i.name)
        table.insert(i)

  ## Store query lineages into a SQLite3 database
  # @param queries, query taxids
  def store_query_lineages(self, queries):
    clades = self.cladistic.get_clades()
    if len(clades) == 0:
      for i in queries:
        self.get_clade_from_taxid(i)
      clades = self.cladistic.get_clades()
    for i in clades:
      if not NcbiTaxonomist.database.tableExists(i):
        table = database.table.clade_table.CladeTable(i, NcbiTaxonomist.database)
        table.create(clades[i])
      if  NcbiTaxonomist.database.get_table(i) != None:
        table = NcbiTaxonomist.database.get_table(i)
        table.insert(clades[i])

  ## Store taxonomy as SQLite3 database in memory
  # @return NcbiTaxaTable instance
  def store_database(self):
    if not NcbiTaxonomist.database.tableExists('ncbitaxa'):
      table = database.table.ncbi_taxa_table.NcbiTaxaTable(NcbiTaxonomist.database)
      table.create()
    table.insert_taxa([NcbiTaxonomist.taxa[x] for x in NcbiTaxonomist.taxa])
    return table

  ## Getter for the NCBI taxa table instance
  # @return NcbiTaxaTable instance
  def get_taxa_table(self):
    if self.database.tableExists('ncbitaxa'):
      return self.database.get_table('ncbitaxa')
    return self.store_database()


  ## Return a taxonomy lineage with clade specifiec rank names only
  #  i.e. ommit 'no rank' taxons
  # @param clade, clade instance
  # @lineage, lineage, list of taxons
  def get_normalized_clade_lineage(self, clade, lineage):
    return clade.get_normalized_lineage(lineage)

  ## Return a taxonomy lineage with taxa with specific ranks
  #  i.e. ommit 'no rank' taxons
  # @lineage, lineage, list of taxons
  def get_normalized_lineage(self, lineage):
    norm = []
    for i in lineage:
      if i.rank != 'no rank':
        norm.append(i)
    return norm

  ## Helper function to dump the taxonomy database
  # @param sink, string, possible filename. Defaults to stdout.
  def dump_database(self, sink=None):
    NcbiTaxonomist.database.dump(sink)

  ## Simple getter for a taxon
  # @param taxid, int, NCBI_taxid
  # @return Taxon instance or None
  def get_taxon(self, taxid):
    if taxid in NcbiTaxonomist.taxa:
      return NcbiTaxonomist.taxa[taxid]
    return None

  ## Simple getter for ranks of a specific clade
  # @param clade, str, clade name
  # @return clade,  clade instance
  def get_clade_ranks(self, clade):
    return NcbiTaxonomist.cladistic.rankmap.get(clade, None)
