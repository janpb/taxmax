#!/usr/bin/python3
# -*- coding: utf-8 -*-
#
#  blast_query.py
#
#  Copyright 2017 The University of Sydney
#  Author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  Description:
#

import hashlib
from ..interval import itree
from ..interval import interval

class BlastQuery:

  def __init__(self, qid=None, title=None, length=0):
    self.id = qid
    self.title = title
    self.length = length
    self.uid = hashlib.sha256(title.encode())

  def get_uid(self):
    return self.uid.hexdigest()

  def dump(self):
    print("Qid: {0}\nId: {1}\nTitle: {2}\nLength: {3}".format(self.qid,
                                                              self.id,
                                                              self.title,
                                                              self.length))
