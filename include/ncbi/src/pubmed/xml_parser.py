#-------------------------------------------------------------------------------
#  \file xml_parser.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.0.2
#  \description
#-------------------------------------------------------------------------------
import xml.etree.ElementTree
from . import article

class NcbiPubmedXmlParser:

  def __init__(self):
    self.affiliations = []
    self.affiliation_map = {}
    self.affiliation_idx = 0
    self.artvalues = {'ISSN':None, 'Volume':None, 'Issue':None, 'Year':None,
                      'Month': None, 'Title' : None, 'ISOAbbreviation' : None,
                      'MedlinePgn' : None, 'Abstract' : None,
                      'ArticleTitle' : None}
    self.authvalues = {'LastName':None, 'ForeName':None, 'Initials':None,
                       'Affiliation' : None, 'CollectiveName': None}


  def parse(self, pubmed_xml, art=article.NcbiPubmedArticle(), author = article.NcbiPubmedArticle.Author()):
    parseArticleDetails = False
    parseAuthors = False
    parsePubmedData = False
    library = {}
    for event, elem in xml.etree.ElementTree.iterparse(pubmed_xml, events=["start", "end"]):
      if elem.tag == 'PubmedArticle':
        if event == 'start':
          art = article.NcbiPubmedArticle()
        if event == 'end':
          library[art.pmid] = art
      if elem.tag == 'PubmedData':
        if event == 'start':
          parsePubmedData = True
        if event == 'end':
          parsePubmedData = False

      if elem.tag == 'Article':
        if event == 'start':
          parseArticleDetails = True
        if event == 'end':
          art.journal.title = self.artvalues.get('Title')
          art.journal.iso_abbrev = self.artvalues.get('ISOAbbreviation')
          art.journal.volume = self.artvalues.get('Volume')
          art.journal.issue = self.artvalues.get('Issue')
          art.journal.pages = self.artvalues.get('MedlinePgn')
          art.journal.issn = self.artvalues.get('ISSN')
          art.journal.pubdate.year = int(self.artvalues.get('Year'))
          art.journal.pubdate.month = self.artvalues.get('Month')
          art.abstract = self.artvalues.get('Abstract')
          art.title = self.artvalues.get('ArticleTitle')
          self.reset_valmap(self.artvalues)
          parseArticleDetails = False
          elem.clear()

      if elem.tag == 'Author':
        if event == 'start':
          parseAuthors = True
          author = art.Author()
        if event == 'end':
          if self.authvalues['CollectiveName'] != None:
            author.lastname = self.authvalues.get('CollectiveName')
          else:
            author.name = self.authvalues.get('ForeName')
            author.lastname = self.authvalues.get('LastName')
            author.initials = self.authvalues.get('Initials')
          art.authors.append(author)
          self.reset_valmap(self.authvalues)
          elem.clear()
          parseAuthors = False

      if parseAuthors:
        if event == 'end':
          if elem.tag in self.authvalues:
            self.authvalues[elem.tag] = elem.text
        if elem.tag == 'Affiliation' and event == 'end':
          if elem.text.split('.')[0] not in self.affiliation_map:
            self.affiliation_map[elem.text.split('.')[0]] = self.affiliation_idx
            self.affiliations.append(elem.text.split('.')[0])
            self.affiliation_idx += 1
          author.affiliations.append(self.affiliation_map[elem.text.split('.')[0]])

      if parseArticleDetails:
        if event == 'end':
          if elem.tag in self.artvalues:
            self.artvalues[elem.tag] = elem.text
          if elem.tag == 'ELocationID' and 'EIdType' in elem.attrib:
            if elem.attrib['EIdType'] == 'doi':
              art.doi = elem.text

      if parsePubmedData:
        if elem.tag == 'ArticleId':
          if 'IdType' in elem.attrib and elem.attrib['IdType'] == 'pubmed':
            art.pmid = int(elem.text)
    return library

  def reset_valmap(self, valmap):
    for i in valmap:
      valmap[i] = None
