# \file srasample.py
# \author Jan Piotr Buchmann <jan.buchmann@mykolab.com>
# \copyright 2018
# \version 0.0.1
# \description Class implementing the SRA Sample accession type (SRS)[0].
#               SRS(SRA sample accession):
#               A Sample is an object that contains the metadata describing the
#               physical sample upon which a sequencing experiment was
#               performed. Imported from BioSample.
# References:
# [0]: https://www.ncbi.nlm.nih.gov/books/NBK56913/#search.what_do_the_different_sra_accessi
#-------------------------------------------------------------------------------

class SraSample:

  def __init__(self):
    self.accession = None
    self.name = None
    self.srx = {}
    self.organism = None
    self.sex = None
    self.type = None

  def new(self):
    return SraSample()
