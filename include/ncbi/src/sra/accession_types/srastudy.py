#-------------------------------------------------------------------------------
# \file srastudy.py
# \author Jan Piotr Buchmann <jan.buchmann@mykolab.com>
# \copyright 2018
# \version 0.0.1
# \description Class implementing the SRA Study accession type (SRP)[0].
#               SRP (SRA study accession):
#                 A Study is an object that contains the project metadata
#                 describing a sequencing study or project. Imported from
#                 BioProject.
# References:
# [0]: https://www.ncbi.nlm.nih.gov/books/NBK56913/#search.what_do_the_different_sra_accessi
#-------------------------------------------------------------------------------

class SraStudy:

  def __init__(self):
    self.accession = None
    self.bioprojects = {}
    self.srx = {}

  def link(self, srx, bioproject):
    self.srx[srx.accession] = srx
    srx.srps[self.accession] = self
    self.bioprojects[bioproject.accession] = bioproject

  def new(self):
    return SraStudy()
