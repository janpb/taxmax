#-------------------------------------------------------------------------------
#  \file genbank_feature.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.0.0
#  \description
#-------------------------------------------------------------------------------

class GenbankFeature:

  def __init__(self):
    self.start = 0
    self.end = 0
    self.accession = None
    self.qualifier = {}
