#-------------------------------------------------------------------------------
#  \file genbank_reference.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.0.0
#  \description
#-------------------------------------------------------------------------------

class GenbankReference:

  def __init__(self):
    self.reference = 0
    self.reference_from = 0
    self.reference_to = 0
    self.authors = {}
    self.title = None
    self.journal = None
    self.xref = {}
