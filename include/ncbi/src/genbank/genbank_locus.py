#-------------------------------------------------------------------------------
#  \file locus.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.0.0
#  \description
#-------------------------------------------------------------------------------

class GenbankLocus:

  class Accession:

    def __init__(self):
      self.primary = None
      self.version = None

  def __init__(self):
    self.locus = None
    self.length = 0
    self.moltype = None
    self.strandness = None
    self.topology = None
    self.division = None
    self.definition = None
    self.other_seqids = {}
    self.source = None
    self.organism = None
    self.taxonomy = None
    self.comment = None
    self.sequence = None
