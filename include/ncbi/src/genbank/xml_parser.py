#-------------------------------------------------------------------------------
#  \file xml_parser.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.0.0
#  \description
#-------------------------------------------------------------------------------
import io
import os
import sys
import xml.etree.ElementTree

import genbank_locus

class GenBankXmlParser:

  def __init__(self):
    pass

  def parse(self, xml):
    for event, elem in ET.iterparse(request.response, events=["start", "end"]):
