#-------------------------------------------------------------------------------
#  \file efetcher.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \description Implements efetch requests to NCBI. Inherits from EdFunction.
#  \copyright 2018 The University of Sydney
#  \version 1.0.2
#-------------------------------------------------------------------------------
import io
import sys
import math

from ..edbase import edfunction
from ..edbase import edanalyzer
from . import efetch_request

class Efetcher(edfunction.EdFunction):

  class Parameters:

    def __init__(self, options):
      self.db = options.pop('db', None)
      self.uids = options.pop('id', None)
      self.webenv = options.pop('WebEnv', None)
      self.querykey = options.pop('query_key', None)
      self.rettype = options.pop('rettype', None)
      self.retmode = options.pop('retmode','xml')
      self.strand = options.pop('strand', None)
      self.seqstart = options.pop('seq_start', None)
      self.seqstop = options.pop('seq_stop', None)
      self.complexity = options.pop('complexity', None)
      self.check()
      self.size = self.set_size(options.pop('size', None))

    def check(self):
      if self.db == None:
        sys.exit("Efetch error::Missing parameter: no database name (db). Abort.")
      if self.uids == None and self.webenv == None and self.querykey == None:
        sys.exit("Efetch error::Missing parameter: no ids, query_key or WebEnv. Abort.")

    def set_size(self, size):
      if size != None:
        return int(size)
      if self.uids != None:
        return len(self.uids)
      return 0

  def __init__(self, tool, email, apikey=None):
    super().__init__('efetch.fcgi', tool, email, apikey)
    self.max_request_size = 500
    self.request_size = self.max_request_size

  def get_parameters(self, options):
    if options == None:
      sys.exit("Efetch error::Missing req parameter: no options given. Abort.")
    return self.Parameters(options)

  def set_request_size(self, request_size):
    if request_size < self.request_size:
      self.request_size = request_size

  def fetch(self, options=None, analyzer=edanalyzer.EdAnalyzer()):
    print(options, file=sys.stderr)
    params = self.get_parameters(options)
    self.set_request_size(params.size)
    expected_requests = math.ceil(params.size/self.request_size)
    req_size = self.request_size
    for i in range(expected_requests):
      if i*self.request_size + req_size  > params.size:
        req_size = params.size - (i*self.request_size)
      print(i, expected_requests, (i*self.request_size), req_size, file=sys.stderr)
      self.requests.append(efetch_request.EfetchRequest(i, params, (i*self.request_size), req_size))
    self.fetch_requests(analyzer)
