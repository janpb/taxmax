#-------------------------------------------------------------------------------
#  \file efetch_request.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \description: The EfetchRequest class which inherits edrequest.EdRequest. It
#                assembles Efetch requests and stores the results.
#  \copyright 2017,2018 The University of Sydney
#  \version 1.0.0
#-------------------------------------------------------------------------------
from ..edbase import edrequest

class EfetchRequest(edrequest.EdRequest):

  def __init__(self, request_id, parameters, start, size):
    super().__init__('efetch', request_id)
    self.db = parameters.db
    self.uids = None
    if parameters.uids != None:
      self.uids =  ','.join(str(x) for x in parameters.uids[start:start+size])
    self.rettype = parameters.rettype
    self.retmode = parameters.retmode
    self.retstart = start
    self.retmax = size
    self.strand = parameters.strand
    self.seqstart = parameters.seqstart
    self.seqstop = parameters.seqstop
    self.complexity = parameters.complexity
    self.webenv = parameters.webenv
    self.querykey = parameters.querykey

  def prepare_qry(self):
    qry = self.prepare_base_qry()
    qry.update({'retstart' : self.retstart, 'retmax' : self.retmax})

    if self.retmode != None:
      qry.update({'retmode' : self.retmode})
    if self.rettype != None:
      qry.update({'rettype' : self.rettype})

    if self.strand != None:
      qry.update({'strand' : self.strand})
    if self.seqstart != None:
      qry.update({'seq_start' : self.seqstart})
    if self.seqstop != None:
      qry.update({'seq_stop' : self.seqstop})
    if self.complexity != None:
      qry.update({'complexity' : self.self.complexity})

    if self.uids == None:
      qry.update({'WebEnv' : self.webenv, 'query_key' : self.querykey})
    else:
      qry.update({'id' : self.uids})
    return qry
