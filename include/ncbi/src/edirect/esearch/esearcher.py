#-------------------------------------------------------------------------------
#  \file esearcher.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \description  Class handling search requests to NCBI. Inherits EdFunction.
#                https://www.ncbi.nlm.nih.gov/books/NBK25499/#chapter4.ESearch
#                Requires an analyzer as input.
#  \copyright 2017,2018 The University of Sydney
#  \version 1.0.0
#-------------------------------------------------------------------------------
import io
import sys
from ..edbase import edfunction
from ..edbase import edanalyzer
from . import esearch_request


class Esearcher(edfunction.EdFunction):

  class Parameters:

    def __init__(self, options):
      self.db = options.pop('db', None)
      self.term = options.pop('term', None)
      self.webenv = options.pop('WebEnv', None)
      self.querykey = options.pop('query_key', None)
      self.rettype = options.pop('rettype', 'uilist')
      self.retmax = options.pop('retmax', 10000)
      self.retstart = options.pop('retstart', 0)
      self.retmode = options.pop('retmode','json')
      self.usehistory = options.pop('useHistory', True)
      self.sort = options.pop('sort', None)
      self.field = options.pop('field', None)
      self.datetype = options.pop('datetype', None)
      self.reldate = options.pop('reldate', None)
      self.mindate = options.pop('mindate', None)
      self.maxdate = options.pop('maxdate', None)
      self.check()

    def check(self):
      if self.db == None:
        sys.exit("Esearch error::Missing parameter: no database name (db). Abort.")
      if self.term == None and self.webenv == None and self.querykey == None:
        sys.exit("Esearch error::Missing parameter: no term, query_key or WebEnv. Abort.")

  search_map = {}
  request_map = {}
  def __init__(self, tool, email, apikey=None):
    super().__init__('esearch.fcgi', tool, email, apikey)

  def get_parameters(self, options):
    if options == None:
      sys.exit("Esearch error::Missing req parameter: no options given. Abort.")
    return self.Parameters(options)

  def search(self, options=None, analyzer=edanalyzer.EdAnalyzer()):
    params = self.get_parameters(options)
    request = esearch_request.EsearchRequest(len(Esearcher.search_map), params)
    Esearcher.request_map[request.id] = request
    Esearcher.search_map[request.id] = analyzer
    self.requests.append(request)
    self.fetch_requests(analyzer)
    print('\r\n', end='', file=sys.stderr)

  def show_options(self, db, options):
    print("Tool:     {0}  \n \
           Database: {1}  \n \
           RetMode:  {2}  \n \
           Rettype:  {3}".format(self.tool, db, options['retmode'],
                                 options['rettype']), file=sys.stderr)
