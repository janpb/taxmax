#-------------------------------------------------------------------------------
#  \file esearch_request.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \description The Esearch request class which inherits edrequest.EdRequest. It
#               assembles Esearch requests and stores the results.
#  \copyright 2017,2018 The University of Sydney
#-------------------------------------------------------------------------------
from ..edbase import edrequest

class EsearchRequest(edrequest.EdRequest):

  def __init__(self, rid, parameters):
    super().__init__('esearch', rid)
    self.db = parameters.db
    self.term = parameters.term
    self.retmax = parameters.retmax
    self.rettype = parameters.rettype
    self.retmode = parameters.retmode
    self.retstart = parameters.retstart
    self.usehistory = parameters.usehistory
    self.sort = parameters.sort
    self.field = parameters.field
    self.datetype = parameters.datetype
    self.reldate = parameters.reldate
    self.mindate = parameters.mindate
    self.maxdate = parameters.maxdate
    self.webenv = parameters.webenv
    self.querykey = parameters.querykey

  def prepare_qry(self):
    qry = self.prepare_base_qry()
    qry.update({'term' : self.term,
                'retmax' : self.retmax,
                'retstart' : self.retstart,
                'retmode' : self.retmode,
                'usehistory' : self.usehistory
                })
    if self.webenv != None:
      qry.update({'WebEnv' : self.webenv})
    if self.querykey != None:
      qry.update({'query_key' : self.querykey})
    if self.sort != None:
      qry.update({'sort' : self.sort})
    if self.field != None:
      qry.update({'field' : self.field})
    if self.datetype != None:
      qry.update({'datetype' : self.datetype})
    if self.reldate != None:
      qry.update({'reldate' : self.reldate})
    if self.mindate != None:
      qry.update({'mindate' : self.mindate})
    if self.maxdate != None:
      qry.update({'maxdate' : self.maxdate})
    return qry
