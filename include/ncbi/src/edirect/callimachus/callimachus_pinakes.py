#  -------------------------------------------------------------------------------
#  \file callimachus_query.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.0.0
#  \description
#  -------------------------------------------------------------------------------


import io
import os
import sys
import uuid

class NcbiCallimachusPinakes:

  qry_count = 0
  queries = {}

  @staticmethod
  def show_queries():
    print(NcbiCallimachusPinakes.qry_count)
    print(NcbiCallimachusPinakes.queries)

  class SearchQuery:

    def __init__(self, qrymap, options, dependency):
      self.qrymap = qrymap
      self.qtype = 'esearch'
      self.qid = str(uuid.uuid4())
      self.options = options
      self.dependency = dependency

    def resolve_dependency(self, results):
      if self.dependency != None:
        options = results[self.dependency].result.get_link_parameters()
        options.update(self.options)
        self.options = options

  class LinkQuery:

    def __init__(self, qrymap, options, dependency):
      self.qrymap = qrymap
      self.qtype = 'elink'
      self.qid = str(uuid.uuid4())
      self.options = options
      self.dependency = dependency

    def resolve_dependency(self, results):
      if self.dependency != None:
        options = results[self.dependency].result.get_link_parameters()
        options['dbfrom'] = options['db']
        options.update(self.options)
        self.options = options

  class FetchQuery:

    def __init__(self, qrymap, options, dependency, analyzer):
      self.qrymap = qrymap
      self.qid = str(uuid.uuid4())
      self.qtype = 'efetch'
      self.options = {} if options == None else options
      self.analyzer = analyzer
      self.dependency = dependency

    def resolve_dependency(self, results):
      if self.dependency != None:
        options = results[self.dependency].result.get_link_parameters()
        options.update(self.options)
        self.options = options

  class PostQuery:

    def __init__(self, qrymap, options, dependency):
      self.qrymap = qrymap
      self.qid = str(uuid.uuid4())
      self.qtype = 'epost'
      self.options = {} if options == None else options
      self.dependency = dependency

    def resolve_dependency(self, results):
      if self.dependency != None:
        if self.dependency != None:
          options = results[self.dependency].result.get_link_parameters()
          options.update(self.options)
          self.options = options

  def __init__(self):
    self.queries = []
    self.qrymap = {}
    self.qid = NcbiCallimachusPinakes.qry_count
    NcbiCallimachusPinakes.queries[NcbiCallimachusPinakes.qry_count] = self
    NcbiCallimachusPinakes.qry_count += 1

  def new_search(self, options=None, dependency=None):
    if options == None and dependency == None:
      sys.exit("Callimachus error: search missing required options and/or dependencies. Abort.")
    return self.SearchQuery(self.qrymap, options, dependency)

  def new_link(self, options=None, dependency=None):
    if options == None and dependency == None:
      sys.exit("Callimachus error: link missing required options and/or dependencies. Abort.")
    return self.LinkQuery(self.qrymap, options, dependency)

  def new_fetch(self, options=None, dependency=None, analyzer=None):
    if analyzer == None:
      sys.exit("Callimachus error: fetch requests require an analyzer but none given. Abort.")
    if options == None and dependency == None:
      sys.exit("Callimachus error: fetch missing required options and/or dependencies. Abort.")
    return self.FetchQuery(self.qrymap, options, dependency, analyzer)

  def new_post(self, options=None, dependency=None):
    if options == None and dependency == None:
      sys.exit("Callimachus error: post missing required options and/or dependencies. Abort.")
    return self.PostQuery(self.qrymap, options, dependency)


  def add_query(self, query):
    self.queries.append(query)
    self.qrymap[query.qid] = query
    return query.qid
