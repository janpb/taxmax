#-------------------------------------------------------------------------------
#  \file ncbi_callimachus.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.0.2
#  \description The NcbiCallimachus class facilitates esearch -> elink -> efetch
#-------------------------------------------------------------------------------
import io
import os
import sys

from ..esearch import esearcher
from ..elink import elinker
from ..elink import elink_analyzer
from ..efetch import efetcher
from ..epost import eposter
from ..epost import epost_analyzer

from . import callimachus_search_analyzer
from . import callimachus_pinakes

class NcbiCallimachus:

  def __init__(self, email, tool='ncbiCallimachus'):
    self.tool = tool
    self.email = email
    self.efetch = efetcher.Efetcher(self.tool, email)

  def new_query(self):
    return callimachus_pinakes.NcbiCallimachusPinakes()

  def run_query(self, qry):
    results = {}
    final_result = None
    for i in qry.queries:
      i.resolve_dependency(results)
      if i.qtype == 'esearch':
        results[i.qid] = self.search(i)
      if i.qtype == 'elink':
        results[i.qid] = self.link(i)
      if i.qtype == 'efetch':
        results[i.qid] = self.fetch(i)
      if i.qtype == 'epost':
        results[i.qid] = self.post(i)
      if not results[i.qid].isSuccess():
        print("Error in query {}".format(qry.qid), file=sys.stderr)
        break
      final_result = i.qid
    return results[final_result]

  def search(self, search_qry, analyzer=callimachus_search_analyzer.CallimachusSearchAnalyzer()):
    esearch = esearcher.Esearcher(self.tool, self.email)
    esearch.search(options=search_qry.options, analyzer=analyzer)
    return analyzer

  def link(self, link_qry, analyzer=elink_analyzer.ElinkAnalyzer()):
    linker = elinker.Elinker(self.tool, self.email)
    linker.link(options=link_qry.options, analyzer=analyzer)
    return analyzer

  def post(self, post_query, analyzer=epost_analyzer.EpostAnalyzer()):
    poster = eposter.Eposter(self.tool, self.email)
    poster.post(options=post_query.options, analyzer=analyzer)
    return analyzer

  def fetch(self, fetch_query):
    analyzer = fetch_query.analyzer
    fetcher = efetcher.Efetcher(self.tool, self.email)
    fetcher.fetch(options=fetch_query.options, analyzer=analyzer)
    return analyzer
