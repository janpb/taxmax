#  File: edresult_base.py
#  Author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  Description: Base class for Edirect results
#  Copyright: 2018 The University of Sydney
#  Version: 0

class EdResult:

  def __init__(self, rid):
    self.rid = rid

  def isSuccess(self):
    raise NotImplementedError("Help! Require implementation")

  def dump(self):
    raise NotImplementedError("Help! Require implementation")

  def get_link_parameters(self):
    raise NotImplementedError("Help! Require implementation")
