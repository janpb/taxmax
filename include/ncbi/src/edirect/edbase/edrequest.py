#-------------------------------------------------------------------------------
#  \file edrequest.py
#  \copyright 2017 The University of Sydney
#  \author: Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \version 1.0.1
#-------------------------------------------------------------------------------

class EdRequest:

  def __init__(self, typ, rid):
    self.typ = typ
    self.id = rid
    self.db = None
    self.tool = None
    self.url = None
    self.qry_url = None
    self.contact = None
    self.status = 3 # 0: success, 1: Fail, 3 Queued
    self.request_error = None
    self.size = 1
    self.apikey = None

  def prepare_base_qry(self):
    qry = {'email' : self.contact, 'tool' : self.tool, 'db' : self.db}
    if self.apikey != None:
      qry.update({'api_key' : self.apikey})
    return qry

  def prepare_qry(self):
    raise NotImplementedError("prepare_qry() requires its own implementation")

  def set_status_success(self):
    self.status = 0

  def set_status_fail(self):
    self.status = 1
