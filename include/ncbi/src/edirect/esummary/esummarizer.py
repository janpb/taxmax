#-------------------------------------------------------------------------------
#  \file esummarizer.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.0.0
#  \description
#-------------------------------------------------------------------------------
import sys
import math

from ..edbase import edrequest
from ..edbase import edfunction
from . import esummary_request

class Esummary(edfunction.EdFunction):

  def __init__(self, tool, email, apikey=None):
    super().__init__('esummary.fcgi', tool, email, apikey)


  def summarize(self, analyzer, db, uids, options={}):
    options['db'] = db
    self.set_options(options)
    self.expected_uids = len(uids)
    self.expected_requests = math.ceil(self.expected_uids/self.request_size)
    req_size = self.request_size
    for i in range(self.expected_requests):
      if i*self.request_size + req_size  > self.expected_uids:
        req_size = self.expected_uids - (i*self.request_size)
      req = esummary_request.EsummaryRequest(i, uids, (i*self.request_size),
                                             req_size)
      self.prepare_request(req)
    self.fetch_requests(analyzer)
    print('\r\n', end='', file=sys.stderr)


  def prepare_request(self, request):
    request.db = self.db
    request.typ = self.rettype
    request.mode = self.retmode
    request.apikey = self.retmode
    self.requests.append(request)
