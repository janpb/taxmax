#!/usr/bin/env python3
# -*- coding: utf-8 -*-
#  -------------------------------------------------------------------------------
#  \file esummary_request.py
#  \author Jan P Buchmann <jan.buchmann@sydney.edu.au>
#  \copyright 2018 The University of Sydney
#  \version 0.0.0
#  \description
#  -------------------------------------------------------------------------------

from ..edbase import edrequest

class EsummaryRequest(edrequest.EdRequest):

  def __init__(self, request_id, uids, db, retmax=None, retstart=None, retmode=None,
               webenv=None, querykey=None, apikey=None):
    super().__init__()
    self.id = request_id
    self.db = db
    self.uids = ','.join(str(x) for x in uids[start:start+size])
    self.size = size
    self.retmax = retmax
    self.retmode = retmode
    self.retstart = retstart
    self.querykey = querykey
    self.webenv = webenv



  def prepare_qry(self):
    qry = self.prepare_base_qry()
    qry.update({'id' : self.uids,
                'db' : self.db,
                'retmax' : self.retmax,
                'retstart' : self.retstart})
    return qry
